package org.ccs.cyperweb.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class SqlDao {
	@Autowired
	private JdbcTemplate jdbcTemplate;

	public List executeQuery(String sql) {
		return jdbcTemplate.queryForList(sql, new Object[] {});
	}

	public String executeUpdate(String sql) {
//		return jdbcTemplate.update(sql, new Object[] {});
		if (sql.endsWith(";")) {
			sql = sql.substring(0, sql.length() - 1);
		}

		String[] sqls = sql.split(";");
		int[] result = jdbcTemplate.batchUpdate(sqls);
		
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < result.length; i++) {
			sb.append(result[i]);
			sb.append(",");
		}
		
		return sb.toString();

	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

}
