package org.ccs.cyperweb.service;

import java.util.List;

import org.ccs.cyperweb.dao.SqlDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SqlService {
	@Autowired
	private SqlDao sqlDao;

	public List executeQuery(String sql) {
		return sqlDao.executeQuery(sql);
	}

	@Transactional
	public String executeUpdate(String sql) {
		return sqlDao.executeUpdate(sql);
	}

	public void setSqlDao(SqlDao sqlDao) {
		this.sqlDao = sqlDao;
	}
}
