<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<head>
<title>profile</title>
<script type="text/javascript">
	function confirmDelete(id) {
		showConfirm({
			message : "Are you sure to delete " + id + "?",
			ok : function() {
				window.location = 'profile/delete/' + id;
			}
		});

	}

	function xxx(){
		showBusy();
		var form_data = $('#sql_form').serialize();
		$.post('query.json', form_data, function(data) {
			var r = JSON.stringify(data);
			var $output = $("#output");
			$output.text(r);
		}).done(function() {
			hideBusy();
		});
		
	}	
	
	function yyy(){
		showBusy();
		var form_data = $('#sql_form').serialize();
		$.post('update.json', form_data, function(data) {
			var r = JSON.stringify(data);
			var $output = $("#output");
			$output.text(r);
		}).done(function() {
			hideBusy();
		});
		
	}	
		

</script>
</head>
<body>
	<ul class="breadcrumb">
		<li><a href="${ctx }">Home</a></li>
		<li class="active">SQL</li>
	</ul>

	<div class="container-fluid">
		<div class="row-fluid">
			<c:if test="${not empty success }">
				<div class="alert alert-success">
					<button data-dismiss="alert" class="close" type="button">×</button>
					${success }
				</div>
			</c:if>


			<div class="mywell">
				<form id="sql_form" method="post">
					<textarea name="sql" id="sql" rows="15" cols="100">select * from profile</textarea>
					<p/>
					<input type="button" class="btn" value="Query" onclick="xxx();"/>
					
					
					<input type="button" class="btn" value="Update!" onclick="yyy();"/>
				</form>
				
			</div>
			<div id="output" class="mywell">
				
			</div>

			<%@include file="../include/cyperui.jsp"%>
			
			
			<footer>
				<hr>
				<p class="pull-right">
					A cyper ui by <a href="http://my.oschina.net/uniquejava"
						target="_blank">Cyper</a>
				</p>

				<p>&copy; 2014</p>
			</footer>

		</div>
	</div>

</body>
</html>